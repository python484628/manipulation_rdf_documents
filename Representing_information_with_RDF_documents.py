# -*- coding: utf-8 -*-

# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# November 2020



## How to look for information in RDF documents


# RDFLib is a pure Python package for working with RDF. 
# RDFLib contains most things you need to work with RDF, including:
# parsers and serializers for RDF/XML, N3, NTriples, N-Quads, Turtle, TriX, Trig and JSON-LD
# a Graph interface which can be backed by any one of a number of Store implementations
# store implementations for in-memory, persistent on disk (Berkeley DB) and remote SPARQL endpoints
# a SPARQL 1.1 implementation - supporting SPARQL 1.1 Queries and Update statements
# SPARQL function extension mechanisms

# Install needed package
!pip install rdflib

# Import needed library
import rdflib


# RDFLib aims to be a pythonic RDF API. 
# RDFLib's main data object is a Graph which is a Python collection of RDF 
# Subject, Predicate, Object Triples:
# To create graph and load it with RDF data we use Graph()
# Define g 
g = rdflib.Graph()

# We can parse data from a webpage but in this example we use a local file
# We but the path where the file is and we use parse() function on g (our graph)
g.parse("C:/Users/mario/Desktop/Dekstop_file/M2 Health Data Science 2021 2022/Ontologie Web mining/Djamel Zitouni/Documents/vc-db.rdf")


# We create a function called query to look for something specific in our file
def query(q):
    qres = g.query(q) # We use query() function to be able to use a query
    for row in qres: # For rows in the result of the query research qres
        print("-->".join(row)) # We join all rows
        

# Use of query function to select the subject, the property and the value in the RDF file
query("SELECT ?subject ?property ?value WHERE { ?subject ?property ?value }")


# Use of query function to select the information where there is John Smith in the file
query('SELECT ?x WHERE { ?x <http://www.w3.org/2001/vcard-rdf/3.0#FN>"John Smith" }')
# Result http://somewhere/JohnSmith/


# If we want to start a new line within the query we need to use the backslash
# Example of a query using backlash to select people who have "Smith" as lastname (family)
# It returns the first names of the people who have "Smith" as a last name
query('SELECT ?givenName\
      WHERE { \
             ?y <http://www.w3.org/2001/vcard-rdf/3.0#Family> "Smith" . \
             ?y <http://www.w3.org/2001/vcard-rdf/3.0#Given> ?givenName .\
                 } \
          ')
# Result John and Rebecca

          
# We can also write the query below to have the result as before
# It returns the first names of the people who have "Smith" as a last name
query(' PREFIX vcard:<http://www.w3.org/2001/vcard-rdf/3.0#> \
      SELECT ?givenName\
      WHERE { \
             ?y vcard:Family "Smith" . \
             ?y vcard:Given ?givenName .\
                 } \
          ')
# Result John and Rebecca


# Query to look for people above 24 years old
# We have to precise if integer, character etc otherwise it can gives us false results
query('PREFIX info: <http://somewhere/peopleInfo#> \
      SELECT ?resource \
          WHERE{ \
                ?resource info:age ?age . \
                    FILTER (xsd:integer(?age) >= 24) \
                        } \
              ')          

# Result
# http://somewhere/JohnSmith/
# http://somewhere/SarahJones/
      





      
############# Now we use ontologies from INSEE 
# Link : https://xml.insee.fr/schema/index.html

# Install needed package
!pip install SPARQLWrapper
# Import needed libraries
from SPARQLWrapper import SPARQLWrapper, JSON
import ssl



# Define sparql variable with the link where we will scrape data thanks to SPARQLWrapper
sparql = SPARQLWrapper("https://rdf.insee.fr/sparql")

# Use sparql variable with setQuery to define a query
# In our query we are looking for the subject, property and value
# We limit to 20 otherwise it tales all information from INSEE
sparql.setQuery("""
                SELECT ?subject ?property ?value
                WHERE { ?subject ?property ?value }
                limit 20
                """)

# We return the result in JSON
sparql.setReturnFormat(JSON)

# Define results variable to store results of the query
results = sparql.query().convert()

# We print all results which are in results["results"]["bindings"]
for result in results["results"]["bindings"]:
    print(result)
    



# Another query             
                

# Define sparql variable with the link where we will scrape data thanks to SPARQLWrapper
sparql = SPARQLWrapper("https://rdf.insee.fr/sparql")

# Use sparql variable with setQuery to define a query
# In our query we are looking for the nom and nomlabel where label is "Lille"
sparql.setQuery("""
                PREFIX igeo: <http://rdf.insee.fr/def/geo#>
                SELECT ?nom ?nomlabel
                WHERE {
                    ?nom igeo:nom ?label . FILTER ( str( ?label ) = "Lille").
                    ?nom igeo:nom ?nomlabel
                    }
                """)
 # We return the result in JSON                     
sparql.setReturnFormat(JSON)

# Define results variable to store results of the query
results = sparql.query().convert()

# We print all results which are in results["results"]["bindings"]
for result in results["results"]["bindings"]:
    print(result)         
          




# Another query 

# Define sparql variable with the link where we will scrape data thanks to SPARQLWrapper
sparql = SPARQLWrapper("https://rdf.insee.fr/sparql")

# Use sparql variable with setQuery to define a query
# In our query we are looking for the nom, property and value where label is "Lille"
sparql.setQuery("""
                PREFIX igeo: <http://rdf.insee.fr/def/geo#>
                SELECT ?nom ?property ?value
                WHERE {
                    ?nom igeo:nom ?label . FILTER ( str( ?label ) = "Lille").
                    ?nom igeo:nom ?nomlabel
                    }
                """)

 # We return the result in JSON                     
sparql.setReturnFormat(JSON)

# Define results variable to store results of the query
results = sparql.query().convert()

# We print all results which are in results["results"]["bindings"]
for result in results["results"]["bindings"]:
    print(result)          
          
          


# Another query

# Define sparql variable with the link where we will scrape data thanks to SPARQLWrapper
sparql = SPARQLWrapper("https://rdf.insee.fr/sparql")

# Use sparql variable with setQuery to define a query
# In our query we are looking for the the total population where label is "Lille"
# We use the SUM function because without it it's very long to have the result
sparql.setQuery("""
                PREFIX igeo: <http://rdf.insee.fr/def/geo#>
                PREFIX idemo:<http://rdf.insee.fr/def/demo#>
                SELECT ?nom (SUM(xsd:integer(?popTotale)) as ?tot_pop)
                WHERE {
                    ?nom igeo:nom ?label . FILTER ( str( ?label ) = "Lille").
                    ?nom owl:sameAs ?communeAlias .
                    ?communeAlias idemo:population ?popLeg .
                    ?popLeg idemo:populationTotale ?popTotale
                    }
                group by ?nom
                """)
 # We return the result in JSON                     
sparql.setReturnFormat(JSON)

# Define results variable to store results of the query
results = sparql.query().convert()

# We print all results which are in results["results"]["bindings"]
for result in results["results"]["bindings"]:
    print(result)       





                
                

